From 57b1714706fcdec389c506a6ec68537a2c0fb91f Mon Sep 17 00:00:00 2001
From: Colin Watson <cjwatson@debian.org>
Date: Sun, 9 Feb 2014 16:10:18 +0000
Subject: Various Debian-specific configuration changes

ssh: Set 'SendEnv LANG LC_*' by default (http://bugs.debian.org/264024).
ssh: Enable HashKnownHosts by default to try to limit the spread of ssh worms.

ssh: Include /etc/ssh/ssh_config.d/*.conf.
sshd: Include /etc/ssh/sshd_config.d/*.conf.

sshd: Enable PAM, disable KbdInteractiveAuthentication, and disable PrintMotd.
sshd: Set 'AcceptEnv LANG LC_*' by default.
sshd: Change sftp subsystem path to /usr/lib/openssh/sftp-server.

Document all of this.

Author: Russ Allbery <rra@debian.org>
Forwarded: not-needed
Last-Update: 2021-11-05

Patch-Name: debian-config.patch
---
 readconf.c    |  2 +-
 ssh.1         | 24 ++++++++++++++++++++++++
 ssh_config    |  8 +++++++-
 ssh_config.5  | 26 +++++++++++++++++++++++++-
 sshd_config   | 18 ++++++++++++------
 sshd_config.5 | 29 +++++++++++++++++++++++++++++
 6 files changed, 98 insertions(+), 9 deletions(-)

--- a/ssh_config
+++ b/ssh_config
@@ -17,7 +17,9 @@
 # list of available options, their meanings and defaults, please see the
 # ssh_config(5) man page.
 
-# Host *
+Include /etc/ssh/ssh_config.d/*.conf
+
+Host *
 #   ForwardAgent no
 #   ForwardX11 no
 #   PasswordAuthentication yes
@@ -44,3 +46,5 @@
 #   ProxyCommand ssh -q -W %h:%p gateway.example.com
 #   RekeyLimit 1G 1h
 #   UserKnownHostsFile ~/.ssh/known_hosts.d/%k
+    SendEnv LANG LC_*
+    HashKnownHosts yes
--- a/ssh_config.5
+++ b/ssh_config.5
@@ -71,6 +71,27 @@ Since the first obtained value for each
 host-specific declarations should be given near the beginning of the
 file, and general defaults at the end.
 .Pp
+Note that the Debian
+.Ic openssh-client
+package sets several options as standard in
+.Pa /etc/ssh/ssh_config
+which are not the default in
+.Xr ssh 1 :
+.Pp
+.Bl -bullet -offset indent -compact
+.It
+.Cm Include /etc/ssh/ssh_config.d/*.conf
+.It
+.Cm SendEnv No LANG LC_*
+.It
+.Cm HashKnownHosts No yes
+.El
+.Pp
+.Pa /etc/ssh/ssh_config.d/*.conf
+files are included at the start of the system-wide configuration file, so
+options set there will override those in
+.Pa /etc/ssh/ssh_config.
+.Pp
 The file contains keyword-argument pairs, one per line.
 Lines starting with
 .Ql #
--- a/sshd_config
+++ b/sshd_config
@@ -10,6 +10,8 @@
 # possible, but leave them commented.  Uncommented options override the
 # default value.
 
+Include /etc/ssh/sshd_config.d/*.conf
+
 #Port 22
 #AddressFamily any
 #ListenAddress 0.0.0.0
@@ -57,8 +59,9 @@ AuthorizedKeysFile	.ssh/authorized_keys
 #PasswordAuthentication yes
 #PermitEmptyPasswords no
 
-# Change to no to disable s/key passwords
-#KbdInteractiveAuthentication yes
+# Change to yes to enable challenge-response passwords (beware issues with
+# some PAM modules and threads)
+KbdInteractiveAuthentication no
 
 # Kerberos options
 #KerberosAuthentication no
@@ -79,7 +82,7 @@ AuthorizedKeysFile	.ssh/authorized_keys
 # If you just want the PAM account and session checks to run without
 # PAM authentication, then enable this but set PasswordAuthentication
 # and KbdInteractiveAuthentication to 'no'.
-#UsePAM no
+UsePAM yes
 
 #AllowAgentForwarding yes
 #AllowTcpForwarding yes
@@ -88,7 +91,7 @@ AuthorizedKeysFile	.ssh/authorized_keys
 #X11DisplayOffset 10
 #X11UseLocalhost yes
 #PermitTTY yes
-#PrintMotd yes
+PrintMotd no
 #PrintLastLog yes
 #TCPKeepAlive yes
 #PermitUserEnvironment no
@@ -105,8 +108,11 @@ AuthorizedKeysFile	.ssh/authorized_keys
 # no default banner path
 #Banner none
 
+# Allow client to pass locale environment variables
+AcceptEnv LANG LC_*
+
 # override default of no subsystems
-Subsystem	sftp	/usr/libexec/sftp-server
+Subsystem	sftp	/usr/lib/openssh/sftp-server
 
 # Example of overriding settings on a per-user basis
 #Match User anoncvs
--- a/sshd_config.5
+++ b/sshd_config.5
@@ -56,6 +56,33 @@ Arguments may optionally be enclosed in
 .Pq \&"
 in order to represent arguments containing spaces.
 .Pp
+Note that the Debian
+.Ic openssh-server
+package sets several options as standard in
+.Pa /etc/ssh/sshd_config
+which are not the default in
+.Xr sshd 8 :
+.Pp
+.Bl -bullet -offset indent -compact
+.It
+.Cm Include /etc/ssh/sshd_config.d/*.conf
+.It
+.Cm KbdInteractiveAuthentication No no
+.It
+.Cm PrintMotd No no
+.It
+.Cm AcceptEnv No LANG LC_*
+.It
+.Cm Subsystem No sftp /usr/lib/openssh/sftp-server
+.It
+.Cm UsePAM No yes
+.El
+.Pp
+.Pa /etc/ssh/sshd_config.d/*.conf
+files are included at the start of the configuration file, so options set
+there will override those in
+.Pa /etc/ssh/sshd_config.
+.Pp
 The possible
 keywords and their meanings are as follows (note that
 keywords are case-insensitive and arguments are case-sensitive):
